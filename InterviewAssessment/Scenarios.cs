﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method. 
        /// </summary>
        public static string test;
        public string Scenario1(string str) => str;
        //public string Scenario1(string str)
        //{
        //    if (str.Length != 0)
        //        return str;
        //    else
        //        return "";
        //}

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent) 
        {
            int n = 1;

            for (int i = 1; i <= exponent; i++)//fixed
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        //public string Scenario4(Node node) => "";
        public string Scenario4(Node node)
        {
            if (node != null)
            {
                test = node.Text;
                testfunction(node);
            }
            return test;
        }

        public static string testfunction(Node node)
        {
            if (node != null)
            {
                if (node.GetType().ToString() != "InterviewAssessment.Node")
                {
                    if (((Tree)node).Children.ToList().Count != 0)
                    {
                        foreach (var item in ((Tree)node).Children)
                        {

                            test = test + "-" + item.Text;
                            testfunction(item);

                        }

                    }


                }
            }
            return test;
        }
    }

    public class Node
    {
       
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
