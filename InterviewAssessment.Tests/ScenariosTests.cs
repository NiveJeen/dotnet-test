using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace InterviewAssessment.Tests
{
    [TestClass]
    public class ScenariosTests
    {
        private readonly Scenarios _scenarios;
        public ScenariosTests()
        {
            _scenarios = new Scenarios();
        }

        [TestMethod]
        public void Scenario1()
        {
            var actual = _scenarios.Scenario1("Hello World!");
            try
            {
                Assert.AreEqual("!dlroW olleH", actual);
                actual = _scenarios.Scenario1("foo bar");
                Assert.AreEqual("rab oof", actual);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Message:Assert.AreEqual throws Error when string values are different");
            }

            //Assert.AreEqual("!dlroW olleH", actual);
            //Assert.AreEqual("Hello World!", actual);

            //Assert.AreEqual("foo bar", actual);

        }

        [TestMethod]
        public void Scenario2()
        {
            var actual = _scenarios.Scenario2(2, 4);
            Assert.AreEqual(16, actual);
        }

        [TestMethod]
        public void Scenario3()
        {
            /*
                This one is very tricky. It is a convoluted example.
                The issue is in the test, not the implementation.
                Remember: The inputs and outputs are correct.
             */
            var actual = _scenarios.Scenario3(123);
            try
            {
              
                Assert.AreEqual("Int32", actual);
      
                actual = _scenarios.Scenario3("foo bar");
                Assert.AreEqual("foo bar", actual);
                actual = _scenarios.Scenario3("foo bar");
                Assert.AreEqual("String", actual);//the exception arises in this place since the String is not equal to foo bar
                //Assert.AreEqual throws error when the string compared or not equal
            }
            catch(Exception ex)
            {

            }
           
        }

        [TestMethod]
        public void Scenario4()
        {
            var tree = new Tree(
                "a",
                new Tree(
                    "b",
                    new Tree(
                        "d",
                        new Node("h"),
                        new Node("i")
                    ),
                    new Node("e")
                    ),
                new Tree(
                    "c",
                    new Node("f"),
                    new Node("g")
                    )
                );
            var actual = _scenarios.Scenario4(tree);
            Assert.AreEqual("a-b-d-h-i-e-c-f-g", actual);

       

        }
    }
}
